import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
import unicorn
from fastapi import FastAPI, File, UploadFile, HTTPException

load_dotenv()

app = FastAPI()

class Model:
    def __init__(self, model_name, model_stage):
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data):

        predictions = self.model.predict(data)
        return predictions

model = Model("real_estage_lgbm", "Staging")

@app.post("/invocations")
async def create_upload_file(userId: int):

    return "prediction"